"use strict";

require("fennec-addon-preferences-workaround");

var self = require('sdk/self'),
    prefs = require("sdk/preferences/service"),
    myprefs = require("sdk/simple-prefs"),
    buttons = require('sdk/ui/button/action'),
    tabs = require("sdk/tabs"),
    base64 = require("sdk/base64"),
    unload = require("sdk/system/unload");

function setCustomServers(hostname, secureAuthPage) {
    var authURIs;

    if (typeof hostname === "undefined" || hostname === null || hostname === false || hostname === "") {
        // Firefox Accounts
        prefs.reset("identity.fxaccounts.auth.uri");
        prefs.reset("identity.fxaccounts.remote.signin.uri");
        prefs.reset("identity.fxaccounts.remote.signup.uri");
        prefs.reset("identity.fxaccounts.remote.force_auth.uri");
        prefs.reset("identity.fxaccounts.settings.uri");
        authURIs = {
            login: prefs.get("identity.fxaccounts.auth.uri") + "/account/login",
            signin: null,
            force_auth: null
        };

        // Firefox Sync
        prefs.reset("identity.sync.tokenserver.uri");

        // Some extras (not listed in wiki)
        prefs.reset("identity.fxaccounts.remote.profile.uri");
        prefs.reset("identity.fxaccounts.remote.oauth.uri");
        prefs.reset("identity.fxaccounts.remote.webchannel.uri");
    } else {
        // Firefox Accounts
        var authURI = "https://$$/v1".replace("$$", hostname);
        authURIs = {
            login: authURI + "/account/login",
            signin: "https://$$/signin?service=sync".replace("$$", hostname),
            force_auth: "https://$$/force_auth?service=sync".replace("$$", hostname)
        };
        prefs.set("identity.fxaccounts.auth.uri", authURI);
        if (!secureAuthPage) {
            prefs.set("identity.fxaccounts.remote.signin.uri", authURIs.signin);
            prefs.set("identity.fxaccounts.remote.force_auth.uri", authURIs.force_auth);
        }
        prefs.set("identity.fxaccounts.remote.signup.uri", "https://$$/signup?service=sync".replace("$$", hostname));
        prefs.set("identity.fxaccounts.settings.uri", "https://$$/settings".replace("$$", hostname));

        // Firefox Sync
        prefs.set("identity.sync.tokenserver.uri", "https://$$/1.0/sync/1.5".replace("$$", hostname));

        // Some extras (not listed in wiki)
        prefs.set("identity.fxaccounts.remote.profile.uri", "https://$$/profile/v1".replace("$$", hostname));
        prefs.set("identity.fxaccounts.remote.oauth.uri", "https://$$/oauth/v1".replace("$$", hostname));
        prefs.set("identity.fxaccounts.remote.webchannel.uri", "https://$$/".replace("$$", hostname));
    }

    // Secure authentication page (served locally from the extension's resources, not off the network)
    // The remote server must allow cross-domain HTTP requests.
    if (secureAuthPage) {
        var secureAuthURI = self.data.url("signin.html") + "#" + base64.encode(JSON.stringify(authURIs));
        prefs.set("identity.fxaccounts.remote.signin.uri", secureAuthURI);
        prefs.set("identity.fxaccounts.remote.force_auth.uri", secureAuthURI);

        // This is misleading, we want to allow "resource://", not "http://" here,
        // but FxAccounts.jsm has no idea about this (and not sure if we can
        // monkey-patch it and remain sane, so... here we go)
        prefs.set("identity.fxaccounts.allowHttp", true);
    } else {
        prefs.reset("identity.fxaccounts.allowHttp");
    }
}

function handleClick(state) {
    setCustomServers(myprefs.prefs.hostname || "", myprefs.prefs.secureAuthPage);
    tabs.open({url: "about:preferences?entrypoint=menupanel#sync"});
}

myprefs.on("applyNow", handleClick);

try {
    var button = buttons.ActionButton({
        id: "set-custom-servers",
        label: "Set custom Accounts and Sync servers",
        icon: {
            "16": "./icon-16.png",
            "32": "./icon-32.png",
            "64": "./icon-64.png",
            "128": "./icon-128.png",
        },
        onClick: handleClick
    });
} catch(e) {
    console.warn("Failed to add ActionButton: %o", e);
}

unload.when(function(reason) {
    if (reason === "disable" || reason == "uninstall") {
        // We can't reset every changes we do (if Sync is active it would be disasterous),
        // but we must reconfigure the authentication pages, because otherwise it would be bad.
        // TODO: NOTE: The pages are not re-enabled when we are.
        var ourSigninURI = self.data.url("signin.html"),
            hadOurURI = false;
        for (var name of ["signin", "force_auth"]) {
            var prefName = "identity.fxaccounts.remote." + name + ".uri",
                uri = prefs.get(prefName);
            if (uri.startsWith(ourSigninURI)) {
                hadOurURI = true;
                try {
                    var data = JSON.parse(base64.decode(uri.slice(s.indexOf("#") + 1)));
                    if (data[name]) {
                        prefs.set(prefName, data[name]);
                    } else {
                        prefs.reset(prefName);
                    }
                } catch(_e) {
                    prefs.reset(prefName);
                }
            }
        }
        if (hadOurURI) {
            prefs.reset("identity.fxaccounts.allowHttp");
        }
    }
});

exports.setCustomServers = setCustomServers;
