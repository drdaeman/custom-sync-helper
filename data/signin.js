"use strict";

(function () {
    function hostname(url) {
        var anchor = document.createElement("a");
        anchor.href = url;
        return anchor.hostname;
    };

    function quickStretchPBKDF2(email, password) {
        var salt = sjcl.codec.utf8String.toBits("identity.mozilla.com/picl/v1/quickStretch:" + email);
        return sjcl.misc.pbkdf2(sjcl.codec.utf8String.toBits(password), salt, 1000, 32 * 8, sjcl.misc.hmac);
    };

    function hkdf(skm, infoName, length) {
        var mac = new sjcl.misc.hmac(sjcl.codec.hex.toBits("00"), sjcl.hash.sha256);
        mac.update(skm);
        var prk = mac.digest();

        var ctxInfo = sjcl.codec.utf8String.toBits("identity.mozilla.com/picl/v1/" + infoName),
            cnt = Math.ceil(length / 32),
            prev = sjcl.codec.hex.toBits(""),
            out = sjcl.codec.hex.toBits("");

        for (var i = 0; i < cnt; i++) {
            var hmac = new sjcl.misc.hmac(prk, sjcl.hash.sha256);
            hmac.update(prev);
            hmac.update(ctxInfo);
            hmac.update(sjcl.codec.utf8String.toBits((String.fromCharCode(i + 1))));
            prev = hmac.digest();
            out = sjcl.bitArray.concat(out, prev);
        }
        return sjcl.bitArray.clamp(out, length * 8);
    };

    function makeSecrets(email, password) {
        var k = quickStretchPBKDF2(email, password);
        return {
            authPW: sjcl.codec.base64.fromBits(hkdf(k, "authPW", 32)),
            unwrapBKey: sjcl.codec.hex.fromBits(hkdf(k, "unwrapBkey", 32))
        };
    };
    // assertEqual(makeSecrets("andr\u00e9@example.org", "p\u00e4ssw\u00f6rd").authPW,
    //             "JHtnX/tMRjELyH4m1xIVOr5eHJDvAKR4RZT5fvVPI3U="");

    var inputEmail = document.getElementById("email"),
        inputPassword = document.getElementById("password"),
        inputShowPassword = document.getElementById("show-password"),
        labelShowPassword = document.getElementById("show-password-label"),
        form = document.getElementById("signin"),
        elServerHost = document.getElementById("server-host");

    labelShowPassword.addEventListener("mousedown", function (event) {
        // Prevent accidental selections by double-clicking
        event.preventDefault();
    });
    inputShowPassword.addEventListener("change", function (event) {
        event.preventDefault();
        inputPassword.type = inputShowPassword.checked ? "text" : "password";
    });
    inputShowPassword.disabled = false;

    var defaultURL = "https://api.accounts.firefox.com/v1",
        url = defaultURL;
    var m = window.location.hash.match(/^#([^\?#;&]+)/);
    if (m) {
        try {
            var urls = JSON.parse(sjcl.codec.utf8String.fromBits(sjcl.codec.base64.toBits(m[1])));
            if (urls.login) { url = urls.login; }
        } catch(_e) { /* ignore */ }
    }
    var urlHostname = hostname(url),
        urlHostnameP = punycode.toASCII(urlHostname);
    if (!urlHostname || !urlHostnameP) {
        url = defaultURL;
        urlHostnameP = urlHostname = hostname(url);
    }
    elServerHost.textContent = urlHostnameP;
    elServerHost.title = urlHostname !== urlHostnameP ? urlHostname : "";

    form.addEventListener("submit", function (event) {
        event.preventDefault();
        var secrets = makeSecrets(inputEmail.value, inputPassword.value);
        var credentials = {
            email: inputEmail.value,
            authPW: secrets.authPW
        };

        var r = new XMLHttpRequest();
        r.open("POST", url);
        r.setRequestHeader("Content-Type", "application/json");
        r.onload = function () {
            if (this.status == 200) {
                var data = JSON.parse(this.responseText);
                data["email"] = credentials.email;
                data["unwrapBKey"] = secrets.unwrapBKey;
                var event = new window.CustomEvent("FirefoxAccountsCommand", {
                    detail: {
                        command: "login",
                        data: data,
                        bubbles: true
                    }
                });
                window.dispatchEvent(event);
            }
        };
        r.send(JSON.stringify(credentials));
    });

    /* window.addEventListener("message", function (event) {
        console.log("[debug] Received message from", event.origin, "with data", event.data);
    }); */
})();
