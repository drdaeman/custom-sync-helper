Custom Sync Helper
==================

This is a tiny extension that simplifies setting up browser
for self-hosted/custom Firefox Accounts and Sync services.

Server configuration
--------------------

When given a custom domain, it assumes that all services are ran
on the same hostname (`https://<hostname>`), as described here:

| Setting                                     | URL             |
|---------------------------------------------|-----------------|
| `identity.fxaccounts.auth.uri`              | `/v1`           |
| `identity.fxaccounts.remote.signin.uri`     | `/signin`       |
| `identity.fxaccounts.remote.force_auth.uri` | `/force_auth`   |
| `identity.fxaccounts.remote.signup.uri`     | `/signup`       |
| `identity.fxaccounts.settings.uri`          | `/settings`     |
| `identity.sync.tokenserver.uri`             | `/1.0/sync/1.5` |
| `identity.fxaccounts.remote.profile.uri`    | `/profile/v1`   |
| `identity.fxaccounts.remote.oauth.uri`      | `/oauth/v1`     |
| `identity.fxaccounts.remote.webchannel.uri` | `/`             |

When given an empty string (the default), all settings are reset
to their default values, switching Firefox back to Mozilla-provided
services.

The configuration is changed only when a toolbar action buttons
is clicked. The primary usage of this extension was testing
custom servers, which required a lot of Firefox restarts. So,
this extension was written to quickly apply the configuration.

Local authentication page
-------------------------

By default, this extension also configures `signin.uri` and
`force_auth.uri` to a locally-hosted authentication page.
This behavior can be disabled in settings.

With the current implementation Firefox fetches the login form
(including all the JS code that implements OnePW standard)
from the network, every time user logs in. The built-in page
implements all the basic necessary crypto locally, avoiding
the possibility that the remote server may possibly inject
malicious code that would leak the password.

The current implementation is quite limited and may not work
for some cases. In particular, two-factor authentication
is not supported.

License
-------

This extension is licensed under GNU Lesser General Public License,
version 3.0 or higher (at your discretion).

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License
as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

This program uses the following third-party components (taken
verbatim, without any modifications) that are distributed under
their own respective licenses:

- [Punycode.js](punycode) by Mathias Bynens,
  distributed under MIT license.
- [Stanford Javascript Crypto Library](sjcl) by Emily Stark et al.,
  distributed dual-licensed under GNU GPLv2+ or BSD.
- [Fira Sans](firasans) font by Mozilla,
  distributed under SIL Open Font License 1.1.
- [Clear Sans](clearsans) font by Intel,
  distributed under Apache 2.0 License.


[punycode]: https://mths.be/punycode
[sjcl]: http://bitwiseshiftleft.github.io/sjcl/
[firasans]: https://github.com/mozilla/Fira
[clearsans]: https://01.org/clear-SANS
